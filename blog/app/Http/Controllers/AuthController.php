<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function signup(){
        return view('signup');
    }

    function welcome(){
        $fn = request('first_name');
        $ln = request('last_name');
        return view('welcome', [
            'nama_depan' => $fn,
            'nama_belakang' => $ln
        ]);
    }
}
