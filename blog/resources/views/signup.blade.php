<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

	<div>
		<h3>Sign Up Form</h3>

		<form action="/welcome" method="GET">
			{{-- @csrf --}}

			<label for="first_name">First name:</label><br><br>
			<input type="text" id="first_name" name="first_name" value="Rifqi"><br><br>

			<label for="last_name">Last name:</label><br><br>
			<input type="text" id="last_name" name="last_name" value="Yahya"><br><br>

			<label>Gender:</label><br><br>
			<input type="radio" id="male" name="gender" value="0" checked>
			<label for="male">Male</label><br>
			<input type="radio" id="female" name="gender" value="1">
			<label for="female">Female</label><br>
			<input type="radio" id="other" name="gender" value="2">
			<label for="other">Rather not say</label><br><br>

			<label>Nationality:</label><br><br>
			<select name="nationality" id="nationality">
				<option value="INA" selected>Indonesian</option>
				<option value="USA">American</option>
				<option value="CHN">Chinese</option>
				<option value="JPN">Japanese</option>
			</select><br><br>

			<label>Language Spoken</label><br><br>
			<input type="checkbox" id="ina" name="language_spoken" value="INA" checked>
			<label for="ina">Bahasa Indonesia</label><br>
			<input type="checkbox" id="eng" name="language_spoken" value="ENG">
			<label for="eng">English</label><br>
			<input type="checkbox" id="oth" name="language_spoken" value="OTH">
			<label for="oth">Other</label><br><br>

			<label for="bio">Bio:</label><br><br>
			<textarea id="bio" placeholder="Input your bio here" rows="6" cols="50" name="bio">Test</textarea>
			<br>

			<input type="Submit" id="submit" value="Sign Up">
		</form>
	</div>

</body>
</html>